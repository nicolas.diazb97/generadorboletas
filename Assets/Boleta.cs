using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boleta
{
    public string nombre;
    public string hash;
    public string ticketType;

    public Boleta(string nombre, string hash, string _ticketType)
    {
        this.nombre = nombre;
        this.hash = hash;
        this.ticketType = _ticketType;
    }
}
