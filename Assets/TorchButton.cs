using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchButton : MonoBehaviour
{
    public GameObject _onSprite;
    public GameObject _offSprite;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Process()
    {
        _onSprite.SetActive(true);
        _offSprite.SetActive(false);
    }
}
