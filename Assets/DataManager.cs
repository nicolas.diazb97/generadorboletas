using Firebase.Database;
using Firebase.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    DatabaseReference reference;
    public QREncodeTest qrData;
    public int minCharAmount;
    public int maxCharAmount;
    // Start is called before the first frame update
    void Start()
    {
        // Get the root reference location of the database.
        reference = FirebaseDatabase.DefaultInstance.RootReference;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetNewHash()
    {
        const string glyphs = "abcdefghijklmnopqrstuvwxyz0123456789";
        string hashToDeliver = "";
        int charAmount = Random.Range(minCharAmount, maxCharAmount); //set those to the minimum and maximum length of your string
        for (int i = 0; i < charAmount; i++)
        {
            hashToDeliver += glyphs[Random.Range(0, glyphs.Length)];
        }
        qrData.uniquekeyCode = hashToDeliver;
        if (qrData.palco.isOn)
        {
            WriteNewUser(qrData.m_inputfield.text, qrData.uniquekeyCode, qrData.m_palco.text + " palco");
        }
        else
        {
            WriteNewUser(qrData.m_inputfield.text, qrData.uniquekeyCode, "general");
        }
    }
    public void WriteNewUser(string _name, string _hash, string _ticketType)
    {
        Boleta user = new Boleta(_name, _hash, _ticketType);
        string json = JsonUtility.ToJson(user);

        reference.Child("users").Child(_hash).SetRawJsonValueAsync(json);
    }
    public void SetState()
    {
        FirebaseDatabase.DefaultInstance
       .GetReference("users")
       .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
       {
           if (e2.DatabaseError != null)
           {
               Debug.LogError(e2.DatabaseError.Message);
           }


           if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
           {

               foreach (var childSnapshot in e2.Snapshot.Children)
               {
                   reference.Child("users").Child(childSnapshot.Child("hash").Value.ToString()).Child("state").SetValueAsync("valido");

                   //var name = childSnapshot.Child("name").Value.ToString();

                   //text.text = name.ToString();
                   //Debug.Log(name.ToString());
                   //text.text = childSnapshot.ToString();

               }

           }

       };
    }
    public void SetUserData(string _hash)
    {
        FirebaseDatabase.DefaultInstance
  .GetReference("users").Child(_hash)
  .GetValueAsync().ContinueWithOnMainThread(task =>
  {
      if (task.IsFaulted)
      {
          //return "error";
          // Handle the error...
          GetComponent<QRDecodeTest>().ShowError("usuario no encontrado");
      }
      else if (task.IsCompleted)
      {
          DataSnapshot snapshot = task.Result;
          if (task.Result.Child("state").Value != null)
          {

              if (task.Result.Child("state").Value.ToString() != "validado")
              {
                  GetComponent<QRDecodeTest>().userName = task.Result.Child("nombre").Value.ToString();
                  GetComponent<QRDecodeTest>().silla = task.Result.Child("ticketType").Value.ToString();
                  GetComponent<QRDecodeTest>().ShowResults();
                  reference.Child("users").Child(_hash).Child("state").SetValueAsync("validado");
              }
              else
              {
                  Debug.Log(task.Result.Child("error").Value);
                  GetComponent<QRDecodeTest>().ShowError("Boleta <color=yellow>ya validada</color>");
              }
          }
          else
          {
              GetComponent<QRDecodeTest>().userName = task.Result.Child("nombre").Value.ToString();
              GetComponent<QRDecodeTest>().silla = task.Result.Child("ticketType").Value.ToString();
              GetComponent<QRDecodeTest>().ShowResults();
              reference.Child("users").Child(_hash).Child("state").SetValueAsync("validado"); ;
          }
          Debug.Log(task.Result.Child("nombre").Value);
          Debug.Log(task.Result.Child("ticketType").Value);
          //Debug.Log("state:" + task.Result.Child("state").Value.ToString());
          // Do something with snapshot...
      }
  });
    }
}
