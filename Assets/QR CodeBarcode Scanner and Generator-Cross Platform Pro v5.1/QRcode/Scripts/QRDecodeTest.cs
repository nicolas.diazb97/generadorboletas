using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TBEasyWebCam;

public class QRDecodeTest : MonoBehaviour
{
    public QRCodeDecodeController e_qrController;

    public Text UiText;

    public GameObject resetBtn;

    public string userName;

    public string silla;

    public GameObject scanLineObj;
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
	bool isTorchOn = false;
#endif
    public Sprite torchOnSprite;
    public Sprite torchOffSprite;
    public Image torchImage;
    public DataManager dataManager;
    public string[] testc;
    /// <summary>
    /// when you set the var is true,if the result of the decode is web url,it will open with browser.
    /// </summary>
    public bool isOpenBrowserIfUrl;

    private void Start()
    {
        if (this.e_qrController != null)
        {
            this.e_qrController.onQRScanFinished += new QRCodeDecodeController.QRScanFinished(this.qrScanFinished);
        }
        //dataManager.SetState();
        //dataManager.SetUserData("jrqowov");
        //this.UiText.text = userName + " asignado en: " + silla;
    }

    private void Update()
    {
    }

    private void qrScanFinished(string dataText)
    {
        if (isOpenBrowserIfUrl)
        {
            if (Utility.CheckIsUrlFormat(dataText))
            {
                if (!dataText.Contains("http://") && !dataText.Contains("https://"))
                {
                    dataText = "http://" + dataText;
                }
                Application.OpenURL(dataText);
            }
        }
        string[] splitArray = dataText.Split(char.Parse("|"));
        dataManager.SetUserData(splitArray[0]);
        if (this.resetBtn != null)
        {
            this.resetBtn.SetActive(true);
        }
        if (this.scanLineObj != null)
        {
            this.scanLineObj.SetActive(false);
        }

    }

    public void ShowResults()
    {
        this.UiText.text = "Nombre: <color=yellow>" + userName + "</color> \n " +
            "Palco: <color=yellow>" + silla + "</color> \n ";
    }
    public void ShowError(string _error)
    {
        this.UiText.text = _error;

    }
    public void Reset()
    {
        if (this.e_qrController != null)
        {
            this.e_qrController.Reset();
        }
        if (this.UiText != null)
        {
            this.UiText.text = string.Empty;
        }
        if (this.resetBtn != null)
        {
            this.resetBtn.SetActive(false);
        }
        if (this.scanLineObj != null)
        {
            this.scanLineObj.SetActive(true);
        }
    }

    public void Play()
    {
        Reset();
        if (this.e_qrController != null)
        {
            this.e_qrController.StartWork();
        }
    }

    public void Stop()
    {
        if (this.e_qrController != null)
        {
            this.e_qrController.StopWork();
        }

        if (this.resetBtn != null)
        {
            this.resetBtn.SetActive(false);
        }
        if (this.scanLineObj != null)
        {
            this.scanLineObj.SetActive(false);
        }
    }

    public void GotoNextScene(string scenename)
    {
        if (this.e_qrController != null)
        {
            this.e_qrController.StopWork();
        }
        //Application.LoadLevel(scenename);
        SceneManager.LoadScene(scenename);
    }

    /// <summary>
    /// Toggles the torch by click the ui button
    /// note: support the feature by using the EasyWebCam Component 
    /// </summary>
    public void toggleTorch()
    {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
		if (EasyWebCam.isActive) {
			if (isTorchOn) {
				torchImage.sprite = torchOffSprite;
				EasyWebCam.setTorchMode (TBEasyWebCam.Setting.TorchMode.Off);
			} else {
				torchImage.sprite = torchOnSprite;
				EasyWebCam.setTorchMode (TBEasyWebCam.Setting.TorchMode.On);
			}
			isTorchOn = !isTorchOn;
		}
#endif
    }
    public void TorchOn(TorchButton torchButton)
    {
        torchButton.Process();
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
		EasyWebCam.setTorchMode(TBEasyWebCam.Setting.TorchMode.On);
#endif
    }
    public void TorchOff(TorchButton torchButton)
    {
        torchButton.Process();
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
		EasyWebCam.setTorchMode(TBEasyWebCam.Setting.TorchMode.Off);
#endif
    }


}
