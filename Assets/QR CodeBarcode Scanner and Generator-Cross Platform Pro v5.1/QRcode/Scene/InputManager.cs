﻿using UnityEngine;
using UnityEngine.Events;

public enum Swipe { None, Up, Down, Left, Right };

public class InputManager : MonoBehaviour
{
    public float minSwipeLength = 20f;
    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    Vector2 oldSwipe;
    public static Swipe swipeDirection;
    public Camera cam;
    public UnityEvent Tap;
    public UnityEvent SwipeUp;
    public UnityEvent SwipeDown;
    public UnityEvent SwipeLeft;
    public UnityEvent SwipeRight;
    public bool swipeMovementAllowed = true;
    public bool tapMovementAllowed;

    private void Start()
    {
    }
    void Update()
    {
        if (swipeMovementAllowed)
            DetectSwipe();
    }

    public void DetectSwipe()
    {
        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Began)
            {
                firstPressPos = new Vector2(t.position.x, t.position.y);
            }

            if (t.phase == TouchPhase.Ended)
            {
                secondPressPos = new Vector2(t.position.x, t.position.y);
                oldSwipe = new Vector3(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
                float angle = 0.2618f;
                float xPrime = oldSwipe.x * Mathf.Cos(angle) - oldSwipe.y * Mathf.Sin(angle);
                float yPrime = oldSwipe.x * Mathf.Sin(angle) + oldSwipe.y * Mathf.Cos(angle);
                currentSwipe = new Vector2(xPrime, yPrime);

                // HACER SWIPE Y NO TAP
                if (currentSwipe.magnitude < minSwipeLength)
                {
                    if (tapMovementAllowed)
                    {
                        Debug.Log("TAP");
                        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                        RaycastHit hit;
                        Physics.Raycast(ray, out hit);
                        Tap.Invoke();
                        //Debug.Log(cam.ScreenToWorldPoint(Input.mousePosition));
                        return;
                    }
                    swipeDirection = Swipe.None;
                    return;
                }

                currentSwipe.Normalize();

                if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                {
                    swipeDirection = Swipe.Up;
                    SwipeUp.Invoke();
                    Debug.Log("up");
                }
                else if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
                {
                    swipeDirection = Swipe.Down;
                    SwipeDown.Invoke();
                    Debug.Log("down");
                }
                else if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                {
                    swipeDirection = Swipe.Left;
                    SwipeLeft.Invoke();
                    Debug.Log("left");
                }
                else if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
                {
                    Debug.Log("right");
                    swipeDirection = Swipe.Right;
                    SwipeRight.Invoke();
                }
            }
        }
        else
        {
            swipeDirection = Swipe.None;
        }
    }
}